<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Project name') }}</title>

    <!-- Scripts -->
    <script src="https://monitoring.legaldesk.uz/assets/js/jquery.min.js"></script>
    <script src="https://monitoring.legaldesk.uz/assets/js/bootstrap.min.js"></script>
    <script src="{{ asset('js/main.js') }}" defer></script>
    <script defer
            src="https://static.cloudflareinsights.com/beacon.min.js/v652eace1692a40cfa3763df669d7439c1639079717194"
            integrity="sha512-Gi7xpJR8tSkrpF7aordPZQlW2DLtzUlZcumS8dMQjwDHEnw9I7ZLyiOj/6tZStRBGtGgN6ceN6cMH8z7etPGlw=="
            data-cf-beacon='{"rayId":"6df53c60ac78376d","token":"cd0b4b3a733644fc843ef0b185f98241","version":"2021.12.0","si":100}'
            crossorigin="anonymous"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
</head>

<body>
<div id="app">
    <div class="wrapper d-flex align-items-stretch">
        <div id="content" class="container p-4">
            <main class="py-4">
                @yield('content')
            </main>
        </div>
</body>

</html>
