<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="description" content="Allery JP - Diet food">
    <meta name="keyword" content="Allery JP - Diet food">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.5.1/main.min.js"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.5.1/main.min.css">
</head>
<body>

<header>
    <div class="header__menu">
        <a href="/en">
            <div class="header__logo__lang__menu">
            <img src="https://www.pngkey.com/png/full/106-1066363_healthy-food-png-healthy-food-cartoon-png.png" alt="">
            <span>アレルギー改善食事療法</span>
        </a>
    </div>
        <nav class="container nav__header">
            <ul class="nav__wrap">
                <li class="nav__item"><a href="#booking" id="#booking" class="nav__link">どんな結果が期待できる</a></li>
                <li class="nav__item"><a href="" class="nav__link">講座の予約</a></li>
                <li class="nav__item__call">
                    <a href="https://www.facebook.com/profile.php?id=570733963"><i class="nav__link__call__facebook bi bi-facebook"></i></a>
                    <a href=""><i class="nav__link__call__instagram bi bi-instagram"></i></a>
                    <a href=""><i class="nav__link__call__whatsapp bi bi-envelope-fill"></i></a>
                </li>
            </ul>
            <ul>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ Config::get('languages')[App::getLocale()] }}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        @foreach (Config::get('languages') as $lang => $language)
                            @if ($lang != App::getLocale())
                                <a class="dropdown-item" href="{{ route('lang.switch', $lang) }}"> {{$language}}</a>
                            @endif
                        @endforeach
                    </div>
                </li>
            </ul>
        </nav>
    </div>
</header>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
