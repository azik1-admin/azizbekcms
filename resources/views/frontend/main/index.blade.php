@extends('frontend.layouts.app')
@section('content')
<div class="header">
    <div class="header__block">
        :毎日のアレルギーの薬を卒業したい方へ<br>ナチュラルフードでアレルギー体質を改善
    </div>
    <div class="header__image">
        <img src="https://storage.googleapis.com/mgf-cdn/1/2020/09/glazed-king-oyster-mushroom-bao-buns.png" alt="">
    </div>
</div>

<!-- Youtube video start-->

<div class="container position-relative">
    <div class="youtube__wrap">
        <div class="youtube__link"><iframe width="100%;" height="100%;" src="https://www.youtube.com/embed/EQep_0qq47s" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
    </div>
</div>

<!-- Youtube video end -->


<!-- Header end -->

<main>
        
    <div class="container block__item">
        <div class="about__item">	
            <div class="row">
                <div class="about__icon__block">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="about__icon__item">
                                <img src="/public/img/kurs1.jpg" alt="Shirin taom" class="about__icon">
                            </div>
                            <h5 class="about__title">美味しい！</h5>
                            <p class="about__icon__text">美味しく食べながらアレルギー体質改善をしていきます。健康的だけではなく、美味しくないと長く続けられない。しかし作り方が簡単ではないと実践できない。そんな悩みを解決します。</p>
                        </div>
                        <div class="col-lg-4">
                            <div class="about__icon__item">
                                <img src="/public/img/kurs3.jpg" alt="Tabiiy vositalar" class="about__icon">
                            </div>
                            <h5 class="about__title">体のデトックス </h5>
                            <p class="about__icon__text">自然の食材の効果で体のデトックスをしていきます。結果的にアレルゲンに反応しにくい体づくりを目指していきます。食べ物だけのアプローチだけではなく、レスケミカル環境作り、レスケミカル買い物のヒント得て行きます。</p>
                        </div>
                        <div class="col-lg-4">
                            <div class="about__icon__item">
                                <img src="/public/img/kurs2.jpg" alt="Zararsiz retsept" class="about__icon">
                            </div>
                            <h5 class="about__title">すぐ実践できるレシピ編</h5>
                            <p class="about__icon__text">知識を教えてもらっても、実践に移せなく、もったいないと思ったことがありませんか？すぐにでも実践に移せるように実習をしていきます。レシピ編ができると買い物も楽になり、自炊できる能力が自然に身についていきます。</p>
    
                        </div>
                    </div>
                </div>
    
                <div class="col-lg-7 order-lg-2 pr-4  mt-4">
                    <h5 class="about__item__title">アレルギー食事療法インストラクター</h5>
                    <div class="about__item__text">
                        <p>ニックネーム：ディーリャ  ウズベキスタン出身。</p>
                        <p>マスクを外している間は会話をお控えください。</p>
                        <p>毎日アレルギーで苦しんでいるのはつらいですよね。アレルギーは改善しないとあきらめていませんか？</p>
                        <p>ウズベキスタン流の食事療法、胃腸の修復しというポイントからアレルゲンに反応しない体づくりを教える講座をやっています。</p>
                        <p>なぜ私が教えられますか？</p>
                        <p>私自身と子供が８年間アレルギー性喘息で苦労していました。</p>
                        <p>医療的な知識を勉強し、実体験と観察と組み合わせ、独自のアレルギー改善食事療法を構築しました。</p>
                        <p>食の知識は今の自分を変えるため、将来の自分を作るためへの経費が安くって、価値の高い投資です。</p>
                        <p>どんなアレルギーでも花粉、アトピー、食物、ハウスダスト、ペットアレルギーでも食事で改善します。</p>
                        <p>アレルギーは薬ではなく、簡単に食事で改善できることが当たり前の世界を作っていきたい。</p>
                    </div>
                </div>
                <div class="col-lg-5 order-lg-1 mt-4">
                    <img src="/public/img/profile.jpg" class="about__item__img" alt="">
                </div>
            </div>
        </div>
    </div><div class="container-fluid block__background">	
        <div class="container block__item">
            <div class="heading__price">
                <h4 class="title__underline">Bizning kurslar</h4>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <img class="img-thumbnail" src="https://www.xmple.com/wallpaper/blue-triangle-green-1920x1080-c2-3a3bdd-1dd434-l2-12-12-a-285-f-10.svg" alt="Card image cap">
                        <div class="card-body">
                        <div class="card-title">
                            <h5 class="card-title border-bottom">オンライン体験会</h5>
                        </div>
                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum ullam rerum hic earum atque pariatur eveniet possimus quibusdam minus, nobis tempora unde ad sunt praesentium ratione velit, commodi numquam at?</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <img class="img-thumbnail" src="https://www.xmple.com/wallpaper/blue-triangle-green-1920x1080-c2-3a3bdd-1dd434-l2-12-12-a-285-f-10.svg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title border-bottom">対面講座</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt explicabo unde asperiores itaque quisquam totam sapiente fuga nemo qui, blanditiis facere, atque sunt nostrum sed laborum, architecto officiis perferendis, culpa.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <img class="img-thumbnail" src="https://www.xmple.com/wallpaper/blue-triangle-green-1920x1080-c2-3a3bdd-1dd434-l2-12-12-a-285-f-10.svg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title border-bottom">オンライン</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Itaque a assumenda dolorum beatae explicabo harum placeat qui quibusdam molestiae ea, accusantium modi, aperiam atque molestias vitae mollitia pariatur in voluptatem!</p>
                        </div>
                    </div>
                </div>
    
            </div>
    
        </div>
    </div><div class="container mt-4"><div class="row"><style>
          .fc .fc-button-primary {
            background-color: #ff9800;
            border-color: #ff9800;
          }
          .fc .fc-button-primary:not(:disabled).fc-button-active, .fc .fc-button-primary:not(:disabled):active {
            background-color: #ff9800;
            border-color: #ff9800;
          }
    
          .fc .fc-col-header-cell-cushion, .fc .fc-daygrid-day-number {
            color: #8bc34a;
          }
    
          .fc-direction-ltr .fc-daygrid-event .fc-event-time {
            color: #fff;
          }
          .fc-daygrid-event-dot {
            border: calc(var(--fc-daygrid-event-dot-width,8px)/ 2) solid #ffffff;
          }
          .fc-direction-ltr .fc-daygrid-event.fc-event-end, .fc-direction-rtl .fc-daygrid-event.fc-event-start {
            background: #ff9800;
          }
          .fc-h-event {
            border: 1px solid var(--fc-event-border-color,#3788d8);
          }
    
          .fc-daygrid-dot-event .fc-event-title {
            color: #fff;
          }
        </style>
        <div class="col-md-8 col-sm-12">
          <div id='calendar'></div>
        </div>
    
        <script>
    
          document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');
    
            var calendar = new FullCalendar.Calendar(calendarEl, {
              initialView: 'dayGridMonth',
              initialDate: '2020-09-07',
              headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay'
              },
              events: [
              {
                title: 'All Day Event',
                start: '2020-09-01'
              },
              {
                title: 'Long Event',
                start: '2020-09-07',
                end: '2020-09-10'
              },
              {
                groupId: '999',
                title: 'Repeating Event',
                start: '2020-09-09T16:00:00'
              },
              {
                groupId: '999',
                title: 'Repeating Event',
                start: '2020-09-16T16:00:00'
              },
              {
                title: 'Conference',
                start: '2020-09-11',
                end: '2020-09-13'
              },
              {
                title: 'Meeting',
                start: '2020-09-12T10:30:00',
                end: '2020-09-12T12:30:00'
              },
              {
                title: 'Lunch',
                start: '2020-09-12T12:00:00'
              },
              {
                title: 'Meeting',
                start: '2020-09-12T14:30:00'
              },
              {
                title: 'Birthday Party',
                start: '2020-09-13T07:00:00'
              },
              {
                title: 'Click for Google',
                url: 'http://google.com/',
                start: '2020-09-28'
              }
              ]
            });
    
            calendar.render();
          });
    
        </script><div class="col-md-4">
        <div class="block__item bg__orange">
            <div class="request__item">
                <h5 class="request__item__heading">Hoziroq buyurtma bering</h5>
                <p class="request__item__text">Yaqin vaqt ichida siz bilan aloqaga chiqamiz</p>
                <form>
                    <div class="form__inputs">
                        <input type="email" placeholder="E-mail *" class="form__inputs__input">
                        <input type="phone" placeholder="Comment *" class="form__inputs__input">
                        <select name="" id="" class="form__inputs__input">
                            <option value="">Cours name 1</option>
                            <option value="">Cours name 2</option>
                            <option value="">Cours name 3</option>
                            <option value="">Cours name 4</option>
                            <option value="">Cours name 5</option>
                            <option value="">Cours name 6</option>
                            <option value="">Cours name 7</option>
                            <option value="">Cours name 8</option>
                        </select>
                    </div>
                    <div class="form__button_menu">
                        <button class="form__button"> Yuborish</button>
                    </div>
                </form>
            </div>
        </div>
    </div></div></div><style>
          .fc .fc-button-primary {
            background-color: #ff9800;
            border-color: #ff9800;
          }
          .fc .fc-button-primary:not(:disabled).fc-button-active, .fc .fc-button-primary:not(:disabled):active {
            background-color: #ff9800;
            border-color: #ff9800;
          }
    
          .fc .fc-col-header-cell-cushion, .fc .fc-daygrid-day-number {
            color: #8bc34a;
          }
    
          .fc-direction-ltr .fc-daygrid-event .fc-event-time {
            color: #fff;
          }
          .fc-daygrid-event-dot {
            border: calc(var(--fc-daygrid-event-dot-width,8px)/ 2) solid #ffffff;
          }
          .fc-direction-ltr .fc-daygrid-event.fc-event-end, .fc-direction-rtl .fc-daygrid-event.fc-event-start {
            background: #ff9800;
          }
          .fc-h-event {
            border: 1px solid var(--fc-event-border-color,#3788d8);
          }
    
          .fc-daygrid-dot-event .fc-event-title {
            color: #fff;
          }
        </style>
        <div class="col-md-8 col-sm-12">
          <div id='calendar'></div>
        </div>
    
        <script>
    
          document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');
    
            var calendar = new FullCalendar.Calendar(calendarEl, {
              initialView: 'dayGridMonth',
              initialDate: '2020-09-07',
              headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay'
              },
              events: [
              {
                title: 'All Day Event',
                start: '2020-09-01'
              },
              {
                title: 'Long Event',
                start: '2020-09-07',
                end: '2020-09-10'
              },
              {
                groupId: '999',
                title: 'Repeating Event',
                start: '2020-09-09T16:00:00'
              },
              {
                groupId: '999',
                title: 'Repeating Event',
                start: '2020-09-16T16:00:00'
              },
              {
                title: 'Conference',
                start: '2020-09-11',
                end: '2020-09-13'
              },
              {
                title: 'Meeting',
                start: '2020-09-12T10:30:00',
                end: '2020-09-12T12:30:00'
              },
              {
                title: 'Lunch',
                start: '2020-09-12T12:00:00'
              },
              {
                title: 'Meeting',
                start: '2020-09-12T14:30:00'
              },
              {
                title: 'Birthday Party',
                start: '2020-09-13T07:00:00'
              },
              {
                title: 'Click for Google',
                url: 'http://google.com/',
                start: '2020-09-28'
              }
              ]
            });
    
            calendar.render();
          });
    
        </script><h3>My sertificates</h3>
    
    <div class="container">
        <div class="row">
            <div class="col">
                <div id="slider-container" class="slider">
                    <div class="slide">
                        <span class="slide__text">My Sertificate</span>
                        <img src="/public/img/sert1.jpg" alt="">
                    </div>
                    <div class="slide">
                        <span class="slide__text">My Sertificate</span>
                        <img src="/public/img/sert2.jpg" alt="">
                    </div>
                    <div class="slide">
                        <span class="slide__text">My Sertificate</span>
                        <img src="/public/img/sert3.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="overlay"></div>
    
    <style>
        h3 {
            text-align: center;
            margin: 2% auto;
            font-size: 1.8rem;
            font-weight: 800;
            color: #444;
        }
        .slider {
            display: flex;
            height: 100%;
            overflow-y: hidden;
            overflow-x: scroll !important;
            padding: 16px;
            transform: scroll(calc(var(--i,0)/var(--n)*-100%));
            scroll-behavior: smooth;
        }
        .slider::-webkit-scrollbar {
            height: 5px;
            width: 150px;
            display: none;
        }
        .slider::-webkit-scrollbar-track {
            background: transparent;
        }
        .slider::-webkit-scrollbar-thumb {
            background: #888;
        }
        .slider::-webkit-scrollbar-thumb:hover {
            background: #555;
        }
        .slider img:hover {
            transform: scale(1.05);
            box-shadow: 10px 10px 10px rgba(0,0,0,0.15);
        }
        .slide {
    
            position: relative;
        }
        .slide img {
            height: 100%;
            width: 94%;
            margin: 0 10px;
            object-fit: cover;
            border-radius: 15px;
            cursor: pointer;
            transition: .25s ease-in-out;
        }
    
        .slide__text {
            color: #cc7e1f;
            position: absolute;
            right: 0;
            top: -8px;
            border: 1px solid ;
            padding: 8px 14px;
            background-color: rgba(255,255,255,0.55);
            border-radius: 4px;
            font-weight: bold;
            z-index: 2;
            box-shadow: 0 1px 3px #888;
        }
    
        .control-prev-btn {
            position: absolute;
            top: 50%;
            left: 0;
            background-color: rgba(255,255,255,0.55);
            height: 100px;
            line-height: 100px;
            width: 45px;
            text-align: center;
            box-shadow: 0 1px 3px #888;
            user-select: none;
            color: #444;
            cursor: pointer;
        }
        .control-next-btn {
            position: absolute;
            top: 50%;
            right: 0;
            background-color: rgba(255,255,255,0.55);
            height: 100px;
            line-height: 100px;
            width: 45px;
            text-align: center;
            box-shadow: 0 1px 3px #888;
            user-select: none;
            color: #444;
            cursor: pointer;
        }
        .slide img.zoomed{
            width: 500px;
            height: 600px;
            position: fixed;
            left: 25%;
            top: 0%;
            z-index: 1000;
            transform: scale(1) translatey(0) !important; 
    
        }
        .overlay{
            position: absolute;
            height: 100%;
            width: 100%;
            background: rgba(0,0,0,.45);
            top: 0;
            display: none;
        }
        .overlay.active{
            display: block;
        }
        @media  only screen and (max-width: 420px) {
            .slider {
                padding: 0;
            }
            .slide {
                padding: 16px 10px;
            }
            .slide img {
                margin: 0;
            }
            .control-prev-btn {
                top: 37%;
            }
            .control-next-btn {
                top: 37%;
            }
        }
    
    </style>
    
    <script>
        
        function prev(){
            document.getElementById('slider-container').scrollLeft -= 270;
        }
    
        function next()
        {
            document.getElementById('slider-container').scrollLeft += 270;
        }
    
    
        $(".slide img").on("click" , function(){
            $(this).toggleClass('zoomed');
            $(".overlay").toggleClass('active');
        })
    
    </script><section id="recent-works">
        <div class="container block__item">
            <div class="row">
    
                <div class="col-md-3">
                    <h4 class="heading__title">「人・文化s</h4>
                        <div class="text-center">
                          <img src="/public/img/profile.jpg" class="rounded-circle img-thumbnail" alt="...">
                      </div></span>
    
                      <p>「人・文化・味」を体験する食旅を
                        プロデュースしている。
                        自然の味を大切にする
                    ウズベキスタン料理教室も運営。
                食で健康を手に入れた人生だからこそ、食で人を幸せにして行きたいと思い、始めた天職です。</p>
    
                  </div>
                  <div class="col-md-9">
                    <h4 class="heading__title">「人・文化s</h4>
                        <div class="row">
                            <div class="col-md-6 mb-2">
                                <div class="card">
                                    <iframe width="100%" height="200px" src="https://www.youtube.com/embed/EQep_0qq47s" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="col-md-6 mb-2">
                                <div class="card">
                                    <iframe width="100%" height="200px" src="https://www.youtube.com/embed/EQep_0qq47s" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="col-md-6 mb-2">
                                <div class="card">
                                    <iframe width="100%" height="200px" src="https://www.youtube.com/embed/EQep_0qq47s" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="col-md-6 mb-2">
                                <div class="card">
                                    <iframe width="100%" height="200px" src="https://www.youtube.com/embed/EQep_0qq47s" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="col-md-6 mb-2">
                                <div class="card">
                                    <iframe width="100%" height="200px" src="https://www.youtube.com/embed/EQep_0qq47s" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="col-md-6 mb-2">
                                <div class="card">
                                    <iframe width="100%" height="200px" src="https://www.youtube.com/embed/EQep_0qq47s" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
    
                </div>
            </div>
            <!--/.row-->
        </div>
    </section><!--/#recent-works-->
    <div class="container block__item">
        <h4 class="title text-center">Yangiliklar</h4>
    <div class="row mb-2">
            <div class="col-md-6">
              <div class="card flex-md-row mb-4 box-shadow h-md-250">
                <div class="card-body d-flex flex-column align-items-start">
                  <strong class="d-inline-block mb-2 text-primary">World</strong>
                  <h3 class="mb-0">
                    <a class="text-dark" href="#">Featured post</a>
                  </h3>
                  <div class="mb-1 text-muted">Nov 12</div>
                  <p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
                  <a href="#">Continue reading</a>
                </div>
                <img class="card-img-right flex-auto d-none d-md-block" data-src="holder.js/200x250?theme=thumb" alt="Thumbnail [200x250]" style="width: 200px; height: 250px;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22250%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20250%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_176740e8023%20text%20%7B%20fill%3A%23eceeef%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A13pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_176740e8023%22%3E%3Crect%20width%3D%22200%22%20height%3D%22250%22%20fill%3D%22%2355595c%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2256.1953125%22%20y%3D%22131%22%3EThumbnail%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true">
              </div>
            </div>
            <div class="col-md-6">
              <div class="card flex-md-row mb-4 box-shadow h-md-250">
                <div class="card-body d-flex flex-column align-items-start">
                  <strong class="d-inline-block mb-2 text-success">Design</strong>
                  <h3 class="mb-0">
                    <a class="text-dark" href="#">Post title</a>
                  </h3>
                  <div class="mb-1 text-muted">Nov 11</div>
                  <p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
                  <a href="#">Continue reading</a>
                </div>
                <img class="card-img-right flex-auto d-none d-md-block" data-src="holder.js/200x250?theme=thumb" alt="Thumbnail [200x250]" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22250%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20250%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_176740e8026%20text%20%7B%20fill%3A%23eceeef%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A13pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_176740e8026%22%3E%3Crect%20width%3D%22200%22%20height%3D%22250%22%20fill%3D%22%2355595c%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2256.1953125%22%20y%3D%22131%22%3EThumbnail%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true" style="width: 200px; height: 250px;">
              </div>
            </div>
          </div>
    </div>
        </main>
@endsection