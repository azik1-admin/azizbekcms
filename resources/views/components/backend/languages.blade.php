<div class="card">
    <div class="card-header">
        @foreach (config('translatable.locales') as $locale)
            <a href="{{ request()->url() }}?language={{ $locale }}"
               class="@if (app()->getLocale() == $locale) btn-danger @endif inline-flex fw-bold border-danger btn p-2 me-3 font-size-16">
                {{ strtoupper($locale) }}
            </a>
        @endforeach
    </div>
</div>
<hr>
