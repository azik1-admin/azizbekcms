@if ($errors->any())
    <div class="card">
        <div class="card-body">
            <div class="alert alert-danger">
                <h6>{{__('validation.error')}}</h6>
                <hr>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif
