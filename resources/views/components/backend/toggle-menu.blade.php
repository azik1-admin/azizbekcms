<button type="button" id="sidebarCollapse" class="btn btn-danger">
    <div id="menuToggle">
        <span></span>
        <span></span>
        <span></span>
    </div>
</button>

<style>
    #menuToggle span {
        display: block;
        width: 20px;
        height: 2px;
        margin-bottom: 5px;
        margin-top: 5px;
        position: relative;
        background: #fff;
        border-radius: 3px;
        z-index: 1;
        transform-origin: 4px 0px;
        transition: transform 0.5s cubic-bezier(0.77, 0.2, 0.05, 1.0),
        background 0.5s cubic-bezier(0.77, 0.2, 0.05, 1.0),
        opacity 0.55s ease;
    }

    #menuToggle span:nth-last-child(2) {
        transform-origin: 0% 100%;
    }
</style>
