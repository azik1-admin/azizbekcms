@extends('backend.layouts.app')

@section('content')

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        @include('components.backend.toggle-menu')
        <div class="ms-4">
            <h2 class="p-0 m-0">{{ __('input.services') }}</h2>
        </div>

        <ul class="nav navbar-nav ms-auto">
            <a href="services/create">
                <li class="btn btn-secondary"> Add </li>
            </a>
        </ul>
    </nav>

@include('components.backend.create_error_messages')

<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
    @foreach($language as $index => $lang)
    <li class="nav-item">
      <a class="nav-link {{ $index == 0 ? 'active' : '' }} border me-2 text-uppercase" id="pills-{{$lang}}-tab" data-toggle="pill" href="#pills-{{$lang}}" role="tab" aria-controls="pills-{{$lang}}" aria-selected="true">{{$lang}}</a>
    </li>
    @endforeach
</ul>
<form action="/admin/services/{{$service->id}}" method="post" enctype="multipart/form-data">
    @method('PUT')
<div class="tab-content card" id="pills-tabContent">
    @foreach($language as $index=>$lang)
    <div class="tab-pane fade {{ $index == 0 ? 'show active' : ''  }}" id="pills-{{$lang}}" role="tabpanel" aria-labelledby="pills-{{$lang}}-tab">
        <div class="card-header font-bold">
            {{ __('admin.main_information') }} | {{ $lang }}
        </div>
        <div class="card-body">
            <div class="form-group mb-2">
                <label class="required" for="title">{{ __('admin.name') }} <span class="font-red">*</span></label>
                <input class="form-control" type="text" name="title[]" id="title"  value="{{$service->translate($lang)->title}}">
            </div>
            <div class="form-group mb-2">
                <label class="required" for="slug">{{ __('admin.url') }} <span class="font-red">*</span></label>
                <input class="form-control" type="text" name="slug[]" id="slug"  value="{{$service->translate($lang)->slug}}">
            </div>
            <div class="form-group mb-2">
                <label for="text">{{ __('admin.text') }} <span class="font-red"> *</span></label>
                <textarea class="form-control" name="text[]" id="text{{$index++}}" cols="80"   value="{{$service->translate($lang)->text}}"></textarea>
            </div>
            <div class="form-group mb-2">
                <label class="required" for="name">{{ __('admin.quote') }} </label>
                <input class="form-control" type="text" name="quote[]" id="quote"  value="{{$service->translate($lang)->quote}}">
            </div>
        </div>
        <div class="card-header font-bold border-top">
            SEO
        </div>
        <div class="card-body">
            <div class="form-group mb-2">
                <label class="required" for="meta_title">{{ __('admin.title') }}</label>
                <input class="form-control" type="text" name="meta_title[]" id="meta_title"   value="{{$service->translate($lang)->meta_title}}">
            </div>
            <div class="form-group mb-2">
                <label class="required" for="meta_description">{{ __('admin.meta_description') }}</label>
                <input class="form-control" type="text" name="meta_description[]" id="meta_description"  value="{{$service->translate($lang)->meta_description}}">
            </div>
            <div class="form-group mb-2">
                <label class="required" for="meta_keywords">{{ __('admin.meta_keywords') }}</label>
                <input class="form-control" type="text" name="meta_keywords[]" id="meta_keywords"  value="{{$service->translate($lang)->meta_keywords}}">
            </div>
        </div>
    </div>
    @endforeach
        <div class="card-header font-bold border-top">
            {{ __('admin.media') }}
        </div>
    <div class="card-body">
        <div class="form-group">
            @csrf
            <input type="file" name="media" class="form-control mb-2" id="media">
            <img src="{{ asset('storage/'.$service->media) }}" alt="" style="width: 250px;">
        </div>

    </div>

    <div class="card-footer">
        <div class="form-group mt-2">
            <button type="submit" class="btn btn-primary">{{ __('admin.save') }}</button>
        </div>
    </div>
</div>
</form>

<script>
    CKEDITOR.replace( 'text0' );
    CKEDITOR.replace( 'text1' );
    CKEDITOR.replace( 'text2' );
    CKEDITOR.replace( 'text3' );
    CKEDITOR.replace( 'text4' );
    CKEDITOR.replace( 'text4' );
    CKEDITOR.replace( 'text4' );
</script>

@endsection
