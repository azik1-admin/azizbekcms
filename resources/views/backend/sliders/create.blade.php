@extends('backend.layouts.app')

@section('content')

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        @include('components.backend.toggle-menu')
        <div class="ms-4">
            <h2 class="p-0 m-0">{{ __('admin.sliders') }}</h2>
        </div>

        <ul class="nav navbar-nav ms-auto">
            <a href="/admin/sliders">
                <li class="btn btn-secondary"> {{ __('admin.back') }} </li>
            </a>
        </ul>
    </nav>

    @include('components.backend.create_error_messages')

<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
    <div class="card w-100">
        <div class="card-header">
    @foreach($languages as $index => $lang)
    <li class="nav-item d-inline-flex">
      <a class="btn border-danger me-2 p-2 fw-bold text-uppercase" id="pills-{{ $lang->code }}-tab" data-toggle="pill" href="#pills-{{ $lang->code }}" role="tab" aria-controls="pills-{{ $lang->code }}" aria-selected="false">{{ $lang->code }}</a>
    </li>
    @endforeach
        </div>
    </div>
</ul>

<form action="/admin/sliders" method="post" enctype="multipart/form-data">
<div class="tab-content card" id="pills-tabContent">
    @foreach($languages as $index=>$lang)
        <div class="tab-pane fade {{ $index == 0 ? 'show active' : '' }}" id="pills-{{ $lang->code }}" role="tabpanel" aria-labelledby="pills-{{ $lang->code }}-tab">
            <input type="hidden" name="locale[]" value="{{ $lang->code }}">
            <div class="card-header font-bold">
                Main information | {{ $lang->name }}
            </div>
            <div class="card-body">
                <div class="form-group mb-2">
                    <label class="required" for="name">{{ __('admin.name') }} <span class="font-red">*</span></label>
                    <input class="form-control" type="name" name="name[]" id="name">
                </div>
                <div class="form-group mb-2">
                    <label for="text">{{ __('admin.text') }} <span class="font-red"> *</span></label>
                    <textarea class="form-control" name="text[]" id="text{{$lang->id}}" cols="80"></textarea>
                </div>
                <div class="form-group mb-2">
                    <label class="required" for="name">{{ __('admin.quote') }} </label>
                    <input class="form-control" type="text" name="quote[]" id="quote">
                </div>
            </div>
            <div class="card-header font-bold border-top">
                SEO
            </div>
            <div class="card-body">
                <div class="form-group mb-2">
                    <label class="required" for="meta_title">{{ __('admin.title') }}</label>
                    <input class="form-control" type="text" name="meta_title[]" id="meta_title">
                </div>
                <div class="form-group mb-2">
                    <label class="required" for="meta_description">{{ __('admin.meta_description') }}</label>
                    <input class="form-control" type="text" name="meta_description[]" id="meta_description">
                </div>
                <div class="form-group mb-2">
                    <label class="required" for="meta_keywords">{{ __('admin.meta_keywords') }}</label>
                    <input class="form-control" type="text" name="meta_keywords[]" id="meta_keywords">
                </div>
            </div>
        </div>
    @endforeach
    <div class="card-footer border-bottom">
        <div class="form-group">
            @csrf
            <label for="media">{{ __('admin.media') }}</label>
            <input type="file" name="media" class="form-control mb-2" id="media">
        </div>
    </div>
        <div class="card-body">
            <button type="submit" class="btn btn-primary">{{ __('admin.save') }}</button>
        </div>
    </div>
</form>

<style>
    .font-red {
        color: #ff0000;
    }

    .font-bold {
        font-weight: bold;
    }

    label, .form-control, color-black {
        color: #484848;
        font-size: 14px;
    }
</style>

<script>
    CKEDITOR.replace( 'text1' );
    CKEDITOR.replace( 'text2' );
    CKEDITOR.replace( 'text3' );
    CKEDITOR.replace( 'text4' );
    CKEDITOR.replace( 'text5' );
</script>

@endsection
