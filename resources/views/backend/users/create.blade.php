@extends('backend.layouts.app')

@section('content')

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        @include('components.backend.toggle-menu')
        <div class="ms-4">
            <h2 class="p-0 m-0">{{ __('admin.sliders') }}</h2>
        </div>

        <ul class="nav navbar-nav ms-auto">
            <a href="/admin/sliders">
                <li class="btn btn-secondary"> {{ __('admin.back') }} </li>
            </a>
        </ul>
    </nav>

    @include('components.backend.create_error_messages')


    <form action="/admin/users" method="post" enctype="multipart/form-data">
        @csrf
        <div class="card">
            <div class="card-body border-bottom">
                <div class="form-group">
                    <label for="media">{{ __('admin.name') }}</label>
                    <input type="text" name="name" class="form-control mb-2" id="name">
                </div>
                <div class="form-group">
                    <label for="media">{{ __('Email Address') }}</label>
                    <input type="email" name="email" class="form-control mb-2" id="email">
                </div>
                <div class="form-group">
                    <label for="media">{{ __('Password') }}</label>
                    <input type="password" name="password" class="form-control mb-2" id="password">
                </div>
            </div>
            <div class="card-body">
                <button type="submit" class="btn btn-primary">{{ __('admin.save') }}</button>
            </div>
        </div>
    </form>

@endsection
