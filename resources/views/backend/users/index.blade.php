@extends('backend.layouts.app')

@section('content')

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        @include('components.backend.toggle-menu')
        <div class="ms-4">
            <h2 class="p-0 m-0">{{ __('admin.users') }}</h2>
        </div>

        <ul class="nav navbar-nav ms-auto">
            <a href="/admin/users/create">
                <li class="btn btn-secondary"> {{ __('admin.add') }} </li>
            </a>
        </ul>
    </nav>

    <div class="card shadow">
        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">{{ __('admin.name') }}</th>
                    <th scope="col">{{ __('admin.email') }}</th>
                    <th scope="col">{{ __('admin.action') }}</th>
                </tr>
                </thead>
                <tbody>
                @php $i = 1;
                @endphp
                @forelse ($users as $item)
                    <tr>
                        <th scope="row" class="col-md-1 col-sm-12">{{ $i++ }}</th>
                        <td class="col-md-4 col-sm-12">{{ $item->name }}</td>
                        <td class="col-md-4 col-sm-12">{{ $item->email }}</td>
                        <td class="col-md-2 col-sm-12">
                            <div class="d-flex">
                                <a href="/admin/users/{{$item->id}}/edit" class="me-2">
                                    <button class="btn btn-warning">{{ __('admin.edit') }}</button>
                                </a>
                                <form method="post" action="/admin/users/{{$item->id}}">
                                    @method("DELETE")
                                    @csrf
                                    <button class="btn btn-danger" type="submit"
                                            onclick="return confirm('Are you sure to delete?')"> {{ __('admin.del') }} </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tbody>
                    <tr>
                        <td colspan="3" class="fw-bold text-center">
                            {{ __('admin.empty') }}, <a href="/admin/users/create">{{ __('admin.add') }}?</a>
                        </td>
                    </tr>
                    </tbody>
                    @endforelse
                    </tbody>
            </table>
        </div>
    </div>


@endsection
