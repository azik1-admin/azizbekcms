@extends('backend.layouts.app')

@section('content')

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        @include('components.backend.toggle-menu')
        <div class="ms-4">
            <h2 class="p-0 m-0">{{ __('input.sliders') }}</h2>
        </div>

        <ul class="nav navbar-nav ms-auto">
            <a href="/admin/sliders/create">
                <li class="btn btn-secondary"> Add </li>
            </a>
        </ul>
    </nav>

    @include('components.backend.create_error_messages')

    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
        @foreach($language as $index => $lang)
            <li class="nav-item">
                <a class="nav-link {{ $index == 0 ? 'active' : '' }} border me-2 text-uppercase" id="pills-{{$lang}}-tab" data-toggle="pill" href="#pills-{{$lang}}" role="tab" aria-controls="pills-{{$lang}}" aria-selected="true">{{$lang}}</a>
            </li>
        @endforeach
    </ul>
    <form action="/admin/sliders/{{$slider->id}}" method="post" enctype="multipart/form-data">
        @method('PUT')
            @foreach($language as $index=>$lang)
                <div class="card">
                    <div class="card-header">
                        User information
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" name="name" value="{{ old('name')  }}">
                        </div>
                        <div class="form-group">
                            <label for="password">Password:</label>
                            <input type="password" name="name" value="{{ old('password')  }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Name:</label>
                            <input type="email" name="email" value="{{ old('email')  }}">
                        </div>
                    </div>
                </div>
            @endforeach

    </form>

@endsection
