<div class="container">
    <div style="position:absolute;z-index:2;right:40%;top:10%;">
        @if(Session::has('message'))
            <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible fade show" id="alertMessageHide">
                {{ Session::get('message') }}
                <button type="button" class="btn-close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        @endif
    </div>
</div>
<script>
    setTimeout(() => {
        let messageHide = document.getElementById('alertMessageHide');
        messageHide.style.display = 'none';
    }, 2000);
</script>
