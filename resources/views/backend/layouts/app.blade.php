<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Project name') }}</title>

    <!-- Scripts -->
    <script src="https://monitoring.legaldesk.uz/assets/js/jquery.min.js"></script>
    <script src="https://monitoring.legaldesk.uz/assets/js/bootstrap.min.js"></script>
    <script src="{{ asset('js/main.js') }}" defer></script>
    <script defer
        src="https://static.cloudflareinsights.com/beacon.min.js/v652eace1692a40cfa3763df669d7439c1639079717194"
        integrity="sha512-Gi7xpJR8tSkrpF7aordPZQlW2DLtzUlZcumS8dMQjwDHEnw9I7ZLyiOj/6tZStRBGtGgN6ceN6cMH8z7etPGlw=="
        data-cf-beacon='{"rayId":"6df53c60ac78376d","token":"cd0b4b3a733644fc843ef0b185f98241","version":"2021.12.0","si":100}'
        crossorigin="anonymous"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
</head>

<body>
    <div id="app">
        <div class="wrapper d-flex align-items-stretch">
            <nav id="sidebar">
                <div class="p-4">
                    <div class="main-logo">
                        <a href="/admin/"> <div>AZIZBEK</div> <span>CMS</span> </a>
                    </div>
                    <ul class="list-unstyled components mb-5">
                        <li>
                            <a href="/admin/services">Services</a>
                        </li>
                        <li>
                            <a href="/admin/sliders">Sliders</a>
                        </li>
                        <li>
                            <a href="/admin/news">News</a>
                        </li>
                        <li>
                            <a href="/admin/articles">Tours</a>
                        </li>
                        <li>
                            <a href="/admin/courses">Cities</a>
                        </li>
                        <li>
                            <a href="/admin/about">Countries</a>
                        </li>
                        <li>
                            <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false"
                                class="dropdown-toggle">Settings</a>
                            <ul class="collapse list-unstyled" id="homeSubmenu">
                                <li>
                                    <a href="/admin/users">Users</a>
                                </li>
                                <li>
                                    <a href="/admin/roles">Roles</a>
                                </li>
                                <li>
                                    <a href="/admin/permissions">Permissions</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <div class="footer">
                        <p>
                            Copyright &copy;<script>
                                document.write(new Date().getFullYear());

                            </script> by Azizbek
                        </p>
                    </div>
                </div>
            </nav>

            <div id="content" class="container p-4">
                @include('backend.layouts.flash-message')


                <main class="py-4">
                    @yield('content')
                </main>
            </div>
</body>

</html>
