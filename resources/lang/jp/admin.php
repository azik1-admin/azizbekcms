<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin panel
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    /* Input fields */

    'name' => 'Name',
    'url' => 'URL',
    'text' => 'Text',
    'quote' => 'Quote',
    'meta_title' => 'Meta title',
    'meta_description' => 'Meta description',
    'meta_keywords' => 'Meta keywords',
    'media' => 'Media',
    'save' => 'Save',
    'back' => 'Back',
    'add' => 'Add',
    'action' => 'Action',
    'edit' => 'Edit',
    'del' => 'Del',

    /* Menu */

    'services' => 'Services'

];
