<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Admin\AboutController;
use App\Http\Controllers\Admin\CalendarController;
use App\Http\Controllers\Admin\CourseController;
use App\Http\Controllers\Admin\NewsController;
use App\Http\Controllers\Admin\ServiceController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::prefix('/admin')->middleware('auth')->group(function () {
    Route::resource('/about', AboutController::class);
    Route::resource('/calendar', CalendarController::class);
    Route::resource('/courses', CourseController::class);
    Route::resource('/news', NewsController::class);
    Route::resource('/services', ServiceController::class);
    Route::resource('/setting', SettingController::class);
    Route::resource('/sliders', SliderController::class);
    Route::resource('/users', UserController::class);
});


Route::group([
    'prefix'=>LaravelLocalization::setLocale(),
    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function() {
    Route::get('/', [App\Http\Controllers\IndexController::class, 'index'])->name('home');
});
