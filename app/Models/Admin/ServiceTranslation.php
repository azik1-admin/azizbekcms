<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceTranslation extends Model
{
    use HasFactory;

    public $fillable = ['title', 'text', 'quote', 'slug', 'meta_title', 'meta_description', 'meta_keywords'];

    public function languages()
    {
        return $this->hasMany(Language::class);
    }

    public $timestamps = false;
}
