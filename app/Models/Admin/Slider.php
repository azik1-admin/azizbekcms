<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use App\Models\Admin\SliderTranslation;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Slider extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable = ['media'];

    public $translatedAttributes = ['meta_title', 'meta_description', 'slug', 'meta_keywords', 'name', 'text', 'quote'];

    public function slider_translated()
    {
        return $this->hasMany(Slidertranslation::class);
    }
}
