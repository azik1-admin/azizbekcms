<?php

namespace App\Models\Admin;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable = ['media'];

    public $translatedAttributes = ['meta_title', 'name', 'meta_description', 'slug', 'meta_keywords', 'text', 'quote'];

}
