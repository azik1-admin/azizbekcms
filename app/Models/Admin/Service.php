<?php

namespace App\Models\Admin;

use App\Models\Language;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\ServiceTranslation;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Illuminate\Support\Facades\App;

class Service extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable = ['media'];

    public $translatedAttributes = ['title', 'slug', 'text', 'quote', 'meta_title', 'meta_description', 'meta_keywords'];

    public function service_translation()
    {
        return $this->hasMany(ServiceTranslation::class);
    }

    public function languages()
    {
        return $this->hasMany(Language::class)->wherePivot('code');;
    }

    public function serviceLang () {
        return $this->hasMany(ServiceTranslation::class, 'service_id')->where('locale','=',App::getLocale());
    }


}
