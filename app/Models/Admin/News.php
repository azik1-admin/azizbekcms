<?php

namespace App\Models\Admin;

use App\Models\Admin\NewsTranslation;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;


class News extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable = ['media'];

    public $translatedAttributes = ['meta_title', 'name', 'meta_description', 'slug', 'meta_keywords', 'text', 'quote'];

    public function news_translated()
    {
        return $this->hasMany(NewsTranslation::class);
    }
}
