<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SliderTranslation extends Model
{
    use HasFactory;

    public $fillable = ['meta_title', 'meta_description', 'slug', 'meta_keywords', 'name', 'text', 'quote'];

    public $timestamps = false;
}
