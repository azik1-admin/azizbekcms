<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SettingTranslation extends Model
{
    use HasFactory;

    public $fillable = ['meta_title', 'meta_description', 'slug', 'name', 'text', 'quote'];

    public $timestamps = false;
}
