<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseTranslation extends Model
{
    use HasFactory;

    public $fillable = ['meta_title', 'meta_description', 'slug', 'keywords', 'name', 'text', 'quote'];

    public $timestamps = false;
}
