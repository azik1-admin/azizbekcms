<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewsTranslation extends Model
{
    use HasFactory;

    public $fillable = ['meta_title', 'name', 'meta_description', 'slug', 'meta_keywords', 'text', 'quote'];

    public $timestamps = false;
}
