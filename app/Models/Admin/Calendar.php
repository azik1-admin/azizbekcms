<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use App\Models\Admin\CalendarTranslation;

class Calendar extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable = ['media'];

    public $translatedAttributes = ['meta_title', 'meta_description', 'slug', 'keywords', 'name', 'text', 'quote'];

    public function calendar_translated()
    {
        return $this->hasMany(CalendarTranslation::class);
    }
}
