<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

use App\Models\Admin\AboutTranslation;

class About extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable = ['media'];

    public $translatedAttributes = ['meta_title', 'meta_description', 'slug', 'meta_keywords', 'name', 'text', 'quote'];

    public function about_translated()
    {
        return $this->hasMany(AboutTranslation::class);
    }

}
