<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\About;
use App\Models\Admin\AboutTranslation;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_en = AboutTranslation::where('locale', '=', 'en')->get();
        $data_jp = AboutTranslation::where('locale', '=', 'jp')->get();
        return view('backend.abouts.index', [
            'data_en' => $data_en,
            'data_jp' => $data_jp
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.abouts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'media' => $request->file('media')->store('abouts', 'public'),
            'en' => [
                'name' => $request->input('name')[0],
                'url' => $request->input('url')[0],
                'text' => $request->input('text')[0],
                'quote' => $request->input('quote')[0],
                'title' => $request->input('title')[0],
                'description' => $request->input('description')[0],
                'keywords' => $request->input('keywords')[0],
            ], 'jp' => [
                'name' => $request->input('name')[1],
                'url' => $request->input('name')[1],
                'text' => $request->input('text')[1],
                'quote' => $request->input('quote')[1],
                'title' => $request->input('title')[1],
                'description' => $request->input('description')[1],
                'keywords' => $request->input('keywords')[1],
            ],

        ];

        About::create($data);
        return redirect('/admin/about');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
