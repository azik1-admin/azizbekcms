<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Slider;
use App\Http\Controllers\Controller;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::translatedIn(app()->getLocale())
            ->latest()
            ->take(10)
            ->get();
        return view('backend.sliders.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        $languages = Language::all();
        return view('backend.sliders.create', compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/admin/sliders/create')
                ->withErrors($validator)
                ->withInput();
        }

        $slider = new Slider();
        $slider->media = $request->file('media')->store('sliders', 'public');
        $slider->save();
        $language = Language::pluck('code')->toArray();
        foreach ($language as $key=>$lang) {
            $slider->translateOrNew($lang)->name = $request->input('name')[$key];
            $slider->translateOrNew($lang)->slug = Str::slug($request->input('name')[$key]);
            $slider->translateOrNew($lang)->text = $request->input('text')[$key];
            $slider->translateOrNew($lang)->quote = $request->input('quote')[$key];
            $slider->translateOrNew($lang)->meta_title = $request->input('meta_title')[$key];
            $slider->translateOrNew($lang)->meta_description = $request->input('meta_description')[$key];
            $slider->translateOrNew($lang)->meta_keywords = $request->input('meta_keywords')[$key];
        }
        $slider->save();

        if(!$slider){
            Session::flash('message', 'Error, something wrong!');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->back();
        }
        Session::flash('message', 'Successfully saved!');
        Session::flash('alert-class', 'alert-success');


        return redirect('/admin/sliders');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Slider  $slider
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::find($id);
        $language = Language::pluck('code')->toArray();
        if ($slider)
            return view('backend.sliders.edit', compact('slider', 'language'));
        else
            return redirect("/admin/sliders")->with(["message"=>"not found!"]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Slider  $slider
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $slider = Slider::find($id);
        $language = Language::pluck('code')->toArray();
        foreach ($language as $key=>$lang){
            $slider->translate($lang)->name = $request->input('name')[$key];
            $slider->translate($lang)->slug = $request->input('slug')[$key];
            $slider->translate($lang)->quote = $request->input('quote')[$key];
            $slider->translate($lang)->text = $request->input('text')[$key];
            $slider->translate($lang)->meta_title = $request->input('meta_title')[$key];
            $slider->translate($lang)->meta_description = $request->input('meta_description')[$key];
            $slider->translate($lang)->meta_keywords = $request->input('meta_keywords')[$key];
        }
        if ($request->file('media')){
            $slider->media = $request->file('media')->store('sliders', 'public');
        }
        $slider->save();

        if(!$slider){
            Session::flash('message', 'Error, something wrong!');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->back();
        }
        Session::flash('message', 'Successfully updated!');
        Session::flash('alert-class', 'alert-success');
        return redirect('/admin/sliders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Slider  $slider
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        if(Slider::destroy($id)) {
            return redirect('/admin/sliders')->with('message', 'Slider has been successfully deleted!');
        } else {
            return redirect('/admin/sliders')->with('message', 'Please try again!');
        }
    }
}
