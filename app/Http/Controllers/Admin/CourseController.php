<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Course;
use App\Models\Language;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|Response
     */
    public function index()
    {
        $courses = Course::translatedIn(app()->getLocale())
            ->latest()
            ->take(10)
            ->get();
        return view('backend.courses.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|Response
     */
    public function create()
    {
        $languages = Language::all();
        return view('backend.courses.create', compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/admin/courses/create')
                ->withErrors($validator)
                ->withInput();
        }

        $course = new Course();
        $course->media = $request->file('media')->store('courses', 'public');
        $course->save();
        $language = Language::pluck('code')->toArray();
        foreach ($language as $key=>$lang) {
            $course->translateOrNew($lang)->name = $request->input('name')[$key];
            $course->translateOrNew($lang)->slug = Str::slug($request->input('name')[$key]);
            $course->translateOrNew($lang)->text = $request->input('text')[$key];
            $course->translateOrNew($lang)->quote = $request->input('quote')[$key];
            $course->translateOrNew($lang)->meta_title = $request->input('meta_title')[$key];
            $course->translateOrNew($lang)->meta_description = $request->input('meta_description')[$key];
            $course->translateOrNew($lang)->meta_keywords = $request->input('meta_keywords')[$key];
        }
        $course->save();

        if(!$course){
            Session::flash('message', 'Error, something wrong!');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->back();
        }
        Session::flash('message', 'Successfully saved!');
        Session::flash('alert-class', 'alert-success');


        return redirect('/admin/courses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        $course = Course::find($id);
        $language = Language::pluck('code')->toArray();
        if ($course)
            return view('backend.courses.edit', compact('course', 'language'));
        else
            return redirect("/admin/courses")->with(["message"=>"not found!"]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(Request $request, $id)
    {

        $course = Course::find($id);
        $language = Language::pluck('code')->toArray();
        foreach ($language as $key=>$lang){
            $course->translate($lang)->name = $request->input('name')[$key];
            $course->translate($lang)->slug = $request->input('slug')[$key];
            $course->translate($lang)->quote = $request->input('quote')[$key];
            $course->translate($lang)->text = $request->input('text')[$key];
            $course->translate($lang)->meta_title = $request->input('meta_title')[$key];
            $course->translate($lang)->meta_description = $request->input('meta_description')[$key];
            $course->translate($lang)->meta_keywords = $request->input('meta_keywords')[$key];
        }
        if ($request->file('media')){
            $course->media = $request->file('media')->store('courses', 'public');
        }
        $course->save();

        if(!$course){
            Session::flash('message', 'Error, something wrong!');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->back();
        }
        Session::flash('message', 'Successfully updated!');
        Session::flash('alert-class', 'alert-success');
        return redirect('/admin/courses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function destroy($id)
    {
        if(Course::destroy($id)) {
            return redirect('/admin/courses')->with('message', 'Course has been successfully deleted!');
        } else {
            return redirect('/admin/courses')->with('message', 'Please try again!');
        }
    }
}
