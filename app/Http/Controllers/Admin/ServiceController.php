<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Service;
use App\Models\Admin\ServiceTranslation;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::translatedIn(app()->getLocale())
            ->latest()
            ->take(10)
            ->get();
        return view('backend.services.index', [
            'services' => $services,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = Language::get();
        return view('backend.services.create', compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $service = new Service();
        $service->media = $request->file('media')->store('service', 'public');
        $service->save();
        $language = Language::pluck('code')->toArray();
        foreach ($language as $key=>$lang) {
            $service->translateOrNew($lang)->title = $request->input('title')[$key];
            $service->translateOrNew($lang)->slug = Str::slug($request->input('title')[$key]);
            $service->translateOrNew($lang)->quote = $request->input('quote')[$key];
            $service->translateOrNew($lang)->text = $request->input('text')[$key];
            $service->translateOrNew($lang)->meta_title = $request->input('meta_title')[$key];
            $service->translateOrNew($lang)->meta_description = $request->input('meta_description')[$key];
            $service->translateOrNew($lang)->meta_keywords = $request->input('meta_keywords')[$key];
        }
        $service->save();

        if(!$service){
            Session::flash('message', 'Error, something wrong!');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->back();
        }
        Session::flash('message', 'Successfully saved!');
        Session::flash('alert-class', 'alert-success');


        return redirect('/admin/services');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::find($id);
        $language = Language::pluck('code')->toArray();
        if ($service)
            return view('backend.services.edit', compact('service', 'language'));
        else
            return redirect("/admin/services")->with(["message"=>"not found!"]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, Language $language)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'slug' => 'required|max:255',
            'media' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $service = Service::find($id);
        $language = Language::pluck('code')->toArray();
        foreach ($language as $key=>$lang){
            $service->translate($lang)->title = $request->input('title')[$key];
            $service->translate($lang)->slug = $request->input('slug')[$key];
            $service->translate($lang)->quote = $request->input('quote')[$key];
            $service->translate($lang)->text = $request->input('text')[$key];
            $service->translate($lang)->meta_title = $request->input('meta_title')[$key];
            $service->translate($lang)->meta_description = $request->input('meta_description')[$key];
            $service->translate($lang)->meta_keywords = $request->input('meta_keywords')[$key];
        }
        if ($request->file('media')){
            $service->media = $request->file('media')->store('service', 'public');
        }
        $service->save();

        if(!$service){
            Session::flash('message', 'Error, something wrong!');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->back();
        }
        Session::flash('message', 'Successfully updated!');
        Session::flash('alert-class', 'alert-success');
        return redirect('/admin/services');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Service::destroy($id)) {
            return redirect('/admin/services')->with('message', 'Service has been successfully deleted!');
        } else {
            return redirect('/admin/services')->with('message', 'Please try again!');
        }
    }
}
