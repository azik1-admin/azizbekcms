<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Language;
use Illuminate\Http\Request;
use App\Models\Admin\News;
use App\Models\Admin\NewsTranslation;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::translatedIn(app()->getLocale())
            ->latest()
            ->take(10)
            ->get();
        return view('backend.news.index', compact('news'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = Language::get();
        return view('backend.news.create', compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/admin/news/create')
                ->withErrors($validator)
                ->withInput();
        }

        $news = new News();
        $news->media = $request->file('media')->store('news', 'public');
        $news->save();
        $language = Language::pluck('code')->toArray();
        foreach ($language as $key=>$lang) {
            $news->translateOrNew($lang)->name = $request->input('name')[$key];
            $news->translateOrNew($lang)->slug = Str::slug($request->input('name')[$key]);
            $news->translateOrNew($lang)->quote = $request->input('quote')[$key];
            $news->translateOrNew($lang)->text = $request->input('text')[$key];
            $news->translateOrNew($lang)->meta_title = $request->input('meta_title')[$key];
            $news->translateOrNew($lang)->meta_description = $request->input('meta_description')[$key];
            $news->translateOrNew($lang)->meta_keywords = $request->input('meta_keywords')[$key];
        }
        $news->save();

        if(!$news){
            Session::flash('message', 'Error, something wrong!');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->back();
        }
        Session::flash('message', 'Successfully saved!');
        Session::flash('alert-class', 'alert-success');


        return redirect('/admin/news');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::find($id);
        $language = Language::pluck('code')->toArray();
        if ($news)
            return view('backend.news.edit', compact('news', 'language'));
        else
            return redirect("/admin/news")->with(["message"=>"not found!"]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'slug' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $news = News::find($id);
        $language = Language::pluck('code')->toArray();
        foreach ($language as $key=>$lang){
            $news->translate($lang)->name = $request->input('name')[$key];
            $news->translate($lang)->slug = $request->input('name')[$key];
            $news->translate($lang)->quote = $request->input('quote')[$key];
            $news->translate($lang)->text = $request->input('text')[$key];
            $news->translate($lang)->meta_title = $request->input('meta_title')[$key];
            $news->translate($lang)->meta_description = $request->input('meta_description')[$key];
            $news->translate($lang)->meta_keywords = $request->input('meta_keywords')[$key];
        }
        if ($request->file('media')){
            $news->media = $request->file('media')->store('news', 'public');
        }
        $news->save();

        if(!$news){
            Session::flash('message', 'Error, something wrong!');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->back();
        }
        Session::flash('message', 'Successfully updated!');
        Session::flash('alert-class', 'alert-success');
        return redirect('/admin/news');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(News::destroy($id)) {
            return redirect('/admin/news')->with('message', 'News has been successfully deleted!');
        } else {
            return redirect('/admin/news')->with('message', 'Please try again!');
        }
    }
}
